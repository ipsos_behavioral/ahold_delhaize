---------------------------------------------------------------------------------------------------
-- Credit Card Data from vendor Sense360. Adhoc script to summarize and report raw data.
-- Jimmy
-- 2020/12/11
---------------------------------------------------------------------------------------------------

-- drop table #spend_per_capita;
SELECT
	cast(a.transaction_ts as date) as transaction_date,
	transaction_year,
	transaction_month,
	state,
	county,
	geography_name,
	ipsos_brand_name,
    ipsos_category,
	county_active_users,
	spend_per_capita,
	sum(total_spend) as total_spend,
	sum(sample) as total_transactions,
	count(row_num) as ct_records
  into #spend_per_capita
  FROM [Ahold2020 ].[Raw].[sense_360_spc] a
  group by
	transaction_ts,
	transaction_year,
	transaction_month,
	state,
	county,
	geography_name,
	geography_name,
	ipsos_brand_name,
    ipsos_category,
	county_active_users,
	spend_per_capita;
-- (1 535 466 rows affected)

select top 100 * from #spend_per_capita;

-- drop table #all_months_spend;
select state, county, ipsos_category, ipsos_brand_name, 
	concat(cast(min(transaction_date) as char(7)),' to ',cast(max(transaction_date) as char(7))) as month_range,
	count(distinct a.transaction_date) as ct_months,
	sum(county_active_users) as county_active_users_all_months,
	sum(total_spend) as total_spend,
	sum(total_transactions) as total_transactions
into #all_months_spend
from #spend_per_capita a
group by  state, county, ipsos_category, ipsos_brand_name;

select top 100 * from #all_months_spend;


--- ALL US
select ipsos_category, ipsos_brand_name,
	max(ct_months) as ct_months_estimate,
	sum(county_active_users_all_months) as county_active_users_all_months,
	sum(total_spend) as total_spend,
	sum(total_transactions) as total_transactions
into #us_all_months_spend
from #all_months_spend
group by ipsos_category, ipsos_brand_name;

select a.*, total_spend/county_active_users_all_months as spend_per_capita
from #us_all_months_spend a
order by ipsos_category, ipsos_brand_name;

-- select top 100 * from #all_months_spend where ipsos_brand_name like 'door%'
select * from #us_all_months_spend;

select * from #all_months_spend
where ipsos_category
in ('Supermarket', 'Discount Grocery', 'Casual Dining', 'Fast Casual', 'Family Midscale', 'Quick Service', 'Fine Dining', 'Pizza');

