---------------------------------------------------------------------------------------------------
-- Credit Card Data from vendor Sense360
-- Script to calculate ranked list of restaurants with change from 2019 to 2020
---------------------------------------------------------------------------------------------------
-- Jimmy
-- 2021/01/20
---------------------------------------------------------------------------------------------------

-- CREDIT CARD DATA
-------------------
-- drop table #spend_per_capita;
SELECT
	cast(a.transaction_ts as date) as transaction_date,
	transaction_year,
	transaction_month,
	state,
	county,
	geography_name,
	ipsos_brand_name,
    ipsos_category,
	county_active_users,
	spend_per_capita,
	sum(total_spend) as total_spend,
	sum(sample) as total_transactions,
	count(row_num) as ct_records
  into #spend_per_capita
  FROM [Ahold2020 ].[Raw].[sense_360_spc] a
  group by
	transaction_ts,
	transaction_year,
	transaction_month,
	state,
	county,
	geography_name,
	geography_name,
	ipsos_brand_name,
    ipsos_category,
	county_active_users,
	spend_per_capita;
-- (1 631 872 rows affected)

-- QA: Update to match census record
update #spend_per_capita set county='St. Mary''s County' from #spend_per_capita where county='St Mary''s County';

delete from #spend_per_capita where state='System View';
-- (5593 rows affected)

select count(*) from #spend_per_capita;
-- 1 626 279

-- System View

create index indx1 on #spend_per_capita (state);
create index indx2 on #spend_per_capita (county);

-- CENSUS
---------
-- select top 100 * from ref.adusa_county_crma_flag;

-- drop table #crma_county_census;
select a.state_abbr, a.county_name,
	concat(a.state_abbr,'.',a.county_name) as geography_name,
	year, pop_estimate, 1 as crma_usa, isnull(adusa_flag,0) as crma_adusa
into #crma_county_census
from core.geo.us_county_census_2017_2020 a
left outer join ref.adusa_county_crma_flag b
	on a.state_abbr=b.state_abbr
	and a.county_name=b.county_name;

select year, crma_usa, crma_adusa,
	count(distinct state_abbr) as ct_states,
	count(*) as ct_geographies,
	sum(pop_estimate) as pop_estimate
from #crma_county_census
group by year, crma_usa, crma_adusa
order by year, crma_adusa;

create index indx1 on #crma_county_census (state_abbr);
create index indx2 on #crma_county_census (county_name);

-- select * from #crma_county_census;
-- select top 100 * from #spend_per_capita;

-- select count(*) from #spend_per_capita
-- 1 631 872

-- Spend Per Capita (weighted to popluation)
--------------------------------------------
-- drop table #spend_per_capita2;
select transaction_year, transaction_month, state, county, ipsos_brand_name, ipsos_category,
	b.pop_estimate as county_census_population,
	b.pop_estimate*spend_per_capita as county_pop_times_spend_per_capita,
	county_active_users as vendor_county_users,
	total_spend as vendor_total_spend,
	total_transactions as vendor_transactions,
	spend_per_capita as spend_per_capita
into #spend_per_capita2
from #spend_per_capita a
left outer join #crma_county_census b
	on a.transaction_year=b.year
	and a.state=b.state_abbr
	and a.county=b.county_name;
-- (1 626 279 rows affected)

-- select * from #spend_per_capita2 where county_census_population is null;

-- CONFIRM 2020 DOES NOT HAVE NOV, DEC DATA
select transaction_year, transaction_month, count(*) ct_records
from #spend_per_capita2
group by transaction_year, transaction_month
order by 1,2;

-- PROJECT 2020, 
insert into #spend_per_capita2
select transaction_year, 11 as transaction_month, state, county, ipsos_brand_name, ipsos_category,
	county_census_population, county_pop_times_spend_per_capita,
	vendor_county_users, vendor_total_spend, vendor_transactions, spend_per_capita
from #spend_per_capita2 a
where transaction_year=2020 and transaction_month=10;

insert into #spend_per_capita2
select transaction_year, 12 as transaction_month, state, county, ipsos_brand_name, ipsos_category,
	county_census_population, county_pop_times_spend_per_capita,
	vendor_county_users, vendor_total_spend, vendor_transactions, spend_per_capita
from #spend_per_capita2 a
where transaction_year=2020 and transaction_month=10;
-- (33449 rows affected)

-- select count(*) from #spend_per_capita2;
-- (1693177 rows affected)

select top 100 * from #spend_per_capita2;

-- select * from #spend_per_capita3;
-- drop table #spend_per_capita3;
select transaction_year, transaction_month, cast('ADUSA' as varchar(30)) as crma,
	ipsos_brand_name, ipsos_category,
	sum(county_census_population) as county_census_population,
	sum(county_pop_times_spend_per_capita) as county_pop_times_spend_per_capita,
	sum(vendor_county_users) as vendor_county_users,
	sum(vendor_total_spend) as vendor_total_spend,
	sum(vendor_transactions) as vendor_transactions,
	sum(spend_per_capita) as spend_per_capita
into #spend_per_capita3
from #spend_per_capita2
group by transaction_year, transaction_month, ipsos_brand_name, ipsos_category;
-- (5839 rows affected)

select ipsos_category, count(*)
from #spend_per_capita3 group by ipsos_category;

select transaction_year, transaction_month, crma, ipsos_brand_name, ipsos_category,
	-- spend_per_capita,
	county_census_population,
	county_pop_times_spend_per_capita/2.5 as spend_hh_projection
from #spend_per_capita3
where ipsos_category in ('Quick Service', 'Family Midscale', 'Pizza', 'Frozen Desserts', 'Fine Dining', 'Casual Dining', 'Fast Casual');



-- END OF FILE --





