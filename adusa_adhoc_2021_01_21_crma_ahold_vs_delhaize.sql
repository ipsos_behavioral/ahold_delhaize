-- load ADUSA CRMA map
-- ahold vsdelhaize

use ahold2020;

select * from ahold2020.ref.adusa_county_crma_flag;
select * from core.geo.us_county_census_2017_2020;

-- select * from #adusa_county_crma_flag;
select a.state_abbr, a.county_name, a.year, a.pop_estimate as population_2019,
	stop_and_shop_flag, giant_food_flag, giant_martin_flag, food_lion_flag, hannaford_flag,
	case when (stop_and_shop_flag=1 or giant_food_flag=1 or giant_martin_flag=1) then 1 else 0 end as agg_ahold_flag,
	case when (food_lion_flag=1 or hannaford_flag=1) then 1 else 0 end as agg_delhaize_flag
into #adusa_county_crma_flag
from core.geo.us_county_census_2017_2020 a
inner join ahold2020.ref.adusa_county_crma_flag b
	on a.state_abbr=b.state_abbr
	and a.county_name=b.county_name
	and a.year=2019;
-- (439 rows affected)

select sum(pop_estimate) from core.geo.us_county_census_2017_2020 where year=2019;


create index indx1 on ahold2020.ref.adusa_county_crma_flag (state_abbr);
create index indx2 on ahold2020.ref.adusa_county_crma_flag (county_name);

select * from ref.adusa_county_crma_flag where county_name like '%mary%';
