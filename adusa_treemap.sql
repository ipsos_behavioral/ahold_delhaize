---------------------------------------------------------------------------------------------------
-- Credit Card Data from vendor Sense360. Script to process Treemap
-- Jimmy
-- 2021/02/09
---------------------------------------------------------------------------------------------------
use [Ahold2020];

-- select count(*) from #spend_per_capita;
-- drop table #spend_per_capita;
SELECT
	cast(a.transaction_ts as date) as transaction_date,
	transaction_year,
	transaction_month,
	state,
	county,
	geography_name,
	ipsos_brand_name,
    ipsos_category,
	county_active_users,
	spend_per_capita as spend_per_capita_old, -- ignore at this level. spend per capita recalculated when aggregated
	sum(total_spend) as total_spend,
	sum(sample) as total_transactions,
	count(row_num) as ct_records
  into #spend_per_capita
  FROM [Ahold2020 ].[Raw].[sense_360_spc] a
  where state != 'System View' and transaction_ts is not null -- QA: Exclude Sense360 topline view
  group by
	transaction_ts,
	transaction_year,
	transaction_month,
	state,
	county,
	geography_name,
	geography_name,
	ipsos_brand_name,
    ipsos_category,
	county_active_users,
	spend_per_capita;
-- (1626279 rows affected)

INSERT INTO #spend_per_capita
SELECT
	cast(a.transaction_ts as date) as transaction_date,
	transaction_year,
	transaction_month,
	state,
	county,
	geography_name,
	ipsos_brand_name,
    ipsos_category,
	county_active_users,
	spend_per_capita as spend_per_capita_old,
	sum(total_spend) as total_spend,
	sum(sample) as total_transactions,
	count(row_num) as ct_records  
  FROM [Ahold2020 ].[Raw].[sense_360_independent] a
  where state != 'System View' and transaction_ts is not null -- QA: Exclude Sense360 topline view
  group by
	transaction_ts,
	transaction_year,
	transaction_month,
	state,
	county,
	geography_name,
	geography_name,
	ipsos_brand_name,
    ipsos_category,
	county_active_users,
	spend_per_capita;
	-- (20057 rows affected)

-- select top 100 * from #spend_per_capita where transaction_month is null;
update #spend_per_capita set county='St. Mary''s County' from #spend_per_capita where county='St Mary''s County';
update #spend_per_capita set transaction_year=year(transaction_date), transaction_month=month(transaction_date)
	from #spend_per_capita a where a.transaction_month is null;

/***
-- drop table #spend_per_capita_brand_name;
select transaction_year,
	transaction_month,
	state,
	county,
	ipsos_category,
	ipsos_brand_name,
	min(county_active_users) as county_active_users_min,
	max(county_active_users) as county_active_users_max,
	sum(total_spend) as total_spend,
	sum(total_transactions) as total_transactions,
	count(*) as ct_records
into #spend_per_capita_brand_name
from #spend_per_capita a
where state != 'System View' -- QA: Sense360 has a topline view
group by
	transaction_year,
	transaction_month,
	state,
	county,
	ipsos_category,
	ipsos_brand_name;
-- (1624955 rows affected)
***/

-- confirm no dupes
-- select * from #spend_per_capita_brand_name where ct_records > 1; --(0 rows affected)
-- select * from #spend_per_capita_brand_name where ipsos_brand_name='Walmart';

/***
select * from #spend_per_capita_brand_name where ipsos_brand_name='Walmart' and county in ('Richmond City', 'Chesapeake City') and transaction_month=11 and transaction_year=2017;
select * from #spend_per_capita_brand_name where county in ('Richmond City', 'Chesapeake City') and transaction_month=11 and transaction_year=2017;
-- transaction_date	transaction_year	transaction_month	state	county	geography_name	ipsos_brand_name	ipsos_category	county_active_users	spend_per_capita	total_spend	total_transactions	ct_records
-- 2017-11-01	2017	11	VA	Chesapeake City	VA.Chesapeake City	7-Eleven	C-Store	13	18.33154	238.309997558594	17	1
-- 2017-11-01	2017	11	VA	Chesapeake city	VA.Chesapeake city	7-Eleven	C-Store	2340	41.60689	97360.1171875	5805	1


select * from #spend_per_capita where county in ('Richmond City', 'Chesapeake City') and transaction_month=11 and transaction_year=2017;

select ipsos_brand_name, transaction_year, state, county, count(*) as ct_records
from #spend_per_capita a
where a.ipsos_brand_name='Walmart' and transaction_year=2017 and transaction_month=11
group by ipsos_brand_name, transaction_year, state, county
order by ct_records desc;
***/

-------------------------------------------------------------------
-- DATA OVERRIDE/REPLACEMENT
-------------------------------------------------------------------
-- Credit Card data does not distiguish GIANT FOOD VS GIANT MARTIN
-- but ADUSA does want this split. We notice that most of the data
-- can be split if we assigned based on county of purchase

-- select * from ref.adusa_county_crma_flag where giant_food_flag=1 and giant_martin_flag=0; -- 27 Giant Food counties
-- select * from ref.adusa_county_crma_flag where giant_food_flag=0 and giant_martin_flag=1; -- 48 Giant Martin counties
-- select * from ref.adusa_county_crma_flag where giant_food_flag=1 and giant_martin_flag=1;
-- select * from core.geo.us_county_census_2017_2020 where state_abbr='MD' and county_name='Carroll County';

-- Giant Food		Zip=21157		Westminster	MD in Carroll County;
-- Giant Martin's		Zip=21784		Sykesville	MD in Carroll County;

-- select * from #spend_per_capita3 where ipsos_brand_name like '%giant food%';
-- select * from #spend_per_capita3 where state is null;

update #spend_per_capita set ipsos_brand_name='Giant Food + Giant Martin''s' from #spend_per_capita where ipsos_brand_name ='Giant Food';
-- (13958 rows affected)

update #spend_per_capita set ipsos_brand_name='Giant Martin''s'
from #spend_per_capita a
inner join ref.adusa_county_crma_flag b
	on a.state=b.state_abbr
	and a.county=b.county_name
	and b.giant_martin_flag=1
	and (county_name != 'Carroll County' or state_abbr != 'MD')
where ipsos_brand_name='Giant Food + Giant Martin''s';
-- (2175 rows affected)

update #spend_per_capita set ipsos_brand_name='Giant Food'
from #spend_per_capita a
inner join ref.adusa_county_crma_flag b
	on a.state=b.state_abbr
	and a.county=b.county_name
	and b.giant_food_flag=1
	and (county_name != 'Carroll County' or state_abbr != 'MD')
where ipsos_brand_name='Giant Food + Giant Martin''s';
-- (1242 rows affected)

-- For DC, DE only Giant Food operates 
update #spend_per_capita set ipsos_brand_name='Giant Food' from #spend_per_capita where ipsos_brand_name='Giant Food + Giant Martin''s' and state in ('DC', 'DE');
-- (46 rows affected)

-- For PA, WV only Giant Martin operates 
update #spend_per_capita set ipsos_brand_name='Giant Martin''s' from #spend_per_capita where ipsos_brand_name='Giant Food + Giant Martin''s' and state in ('PA', 'WV');
-- (236 rows affected)

/***
select ipsos_brand_name, sum(total_spend) as total_spend
from #spend_per_capita3
where transaction_year=2017 and ipsos_brand_name like 'Giant F%' or ipsos_brand_name like 'Giant M%'
group by ipsos_brand_name;
 ***/

 -- checks show that Giant Martin's already make up a 59% raw share based on strong geo matching logic which matches 59% in IRI
 
 -- full script run shows Giant Martin is undercounted. Will assign remaining 2% unattributed to solely Giant Martin's to best match IRI ratios.
update #spend_per_capita set ipsos_brand_name='Giant Martin''s' from #spend_per_capita where ipsos_brand_name='Giant Food + Giant Martin''s';
update #spend_per_capita set ipsos_brand_name='Giant Martin''s' from #spend_per_capita where ipsos_brand_name='Martins';

/*** QUICK SIZING CHECK
select ipsos_category, sum(total_spend) as total_spend
from #spend_per_capita
where transaction_year=2017 and (ipsos_category like '%Independ%' or
	ipsos_category in ('Quick Service', 'Fast Casual', 'Pizza', 'Fine Dining', 'Family Midscale', 'Casual Dining', 'Food Delivery'))
group by ipsos_category;
 ***/

-------------------------------------------------------------------
-- DEDUPE DATA RECORDS
-------------------------------------------------------------------

-- select count(*) from #spend_per_capita;

-- QA: DEDUPE SO THAT CENSUS MAPPING IS UNIQUE
-- Some dupes created from the data replacement exercise
-- Some dupes inherent in data

SELECT	transaction_year,
	transaction_month,
	state,
	county,
	geography_name,
	ipsos_brand_name,
    ipsos_category,
	max(county_active_users) as county_active_users,
	sum(spend_per_capita_old) as spend_per_capita_old,
	sum(total_spend) as total_spend,
	sum(total_transactions) as total_transactions,
	count(*) as ct_records
  into #spend_per_capita2
  from #spend_per_capita
  group by
	transaction_year,
	transaction_month,
	state,
	county,
	geography_name,
	geography_name,
	ipsos_brand_name,
    ipsos_category;
-- (1644507 rows affected)

-- select * from #spend_per_capita2 where ipsos_brand_name like '%independent%';
-- select * from #spend_per_capita3 where transaction_year=2017 and transaction_month=10 and geography_name='NH.Strafford County';
	--county_active_users should stay same acros brands... for same month and year

-- drop table #spend_per_capita3;
SELECT	transaction_year,
	transaction_month,
	state,
	county,
	geography_name,
	ipsos_brand_name,
    ipsos_category,
	county_active_users,
	spend_per_capita_old,
	-- total_spend/county_active_users as spend_per_capita,
	total_spend,
	total_transactions,
	ct_records
  into #spend_per_capita3
  from #spend_per_capita2;
-- (1644507 rows affected)
  
-- these were dupes that got collapsed
-- select * from #spend_per_capita3 where ct_records>1;

-------------------------------------------------------------------
-- CREATE CRMA TABLE
-------------------------------------------------------------------
-- select * from ref.adusa_county_crma_flag;
-- (440 rows affected)

-- select * from #crma_county_census;
-- drop table #crma_county_census;
select a.state_abbr, a.county_name,
	concat(a.state_abbr,'.',a.county_name) as geography_name,
	year, pop_estimate, 1 as crma_usa, isnull(adusa_flag,0) as crma_adusa
into #crma_county_census
from core.geo.us_county_census_2017_2020 a
left outer join ref.adusa_county_crma_flag b
	on a.state_abbr=b.state_abbr
	and a.county_name=b.county_name;

-- check
/***
select year, crma_usa, crma_adusa,
	count(distinct state_abbr) as ct_states,
	-- count(distinct county_name) as ct_counties,
	count(*) as ct_geographies,
	sum(pop_estimate) as pop_estimate
from #crma_county_census
group by year, crma_usa, crma_adusa
order by year, crma_adusa;
***/

-------------------------------------------------------------------
-- category alignment
-------------------------------------------------------------------

-- select * from #spend_per_capita3 where transaction_year=2017 and ipsos_category in ('Online Grocery','Grocery Delivery', 'Meal Kits');

-- select * from #remap_category;
-- drop table #remap_category;
create table #remap_category (
	category_from_data	varchar(300),
	subcategory_ipsos	varchar(300),
	category_ipsos	varchar(300),
	location_type	varchar(300)
);

insert into #remap_category select 'Supermarket','Supermarket','Grocery','In home';
insert into #remap_category select 'Discount Grocery','Discounter','Grocery','In home';

insert into #remap_category select 'Grocery Delivery','Pure-play Online','Grocery Delivery','In home'; -- instacart
insert into #remap_category select 'Online Grocery','Conglomerate Online','Grocery Delivery','In home'; -- amazon fresh
insert into #remap_category select 'Meal Kits','Meal Kits','Grocery Delivery','In home';

insert into #remap_category select 'Supercenter','Supercenter','Supercenter','In home';
insert into #remap_category select 'Club','Club','Club','In home';
insert into #remap_category select 'Pharmacy','Drug','Drug','In home';
insert into #remap_category select 'Dollar','Dollar','Dollar','In home';
insert into #remap_category select 'Liquor','Liquor','Liquor','In home';

insert into #remap_category select 'C-Store','Convenience','Convenience','Out of home';

insert into #remap_category select 'Quick Service','Quick Service','Limited Service','Out of home';
insert into #remap_category select 'Fast Casual','Fast Casual','Limited Service','Out of home';
insert into #remap_category select 'Pizza','Pizza','Limited Service','Out of home';
insert into #remap_category select 'Frozen Desserts','Frozen Desserts','Limited Service','Out of home';

insert into #remap_category select 'Fine Dining','Fine Dining','Full Service','Out of home';
insert into #remap_category select 'Family Midscale','Midscale','Full Service','Out of home';
insert into #remap_category select 'Casual Dining','Casual Dining','Full Service','Out of home';

insert into #remap_category select 'Food Delivery','Food Delivery','Food Delivery','Out of home';
insert into #remap_category select 'Independent Restaurants','Independents','Independents','Out of home';

 
-------------------------------------------------------------------
-- category adjustment factors
-------------------------------------------------------------------

-- select * from #brand_category_edible_by_year where year=2017 order by location_type, category_ipsos, subcategory_ipsos, ipsos_brand_name ;
-- select * from #brand_category_edible_by_year where year=2017 and category_ipsos in ('Grocery Delivery');

-- drop table #brand_category_edible_by_year;
select ipsos_brand_name, transaction_year as year, b.subcategory_ipsos, b.category_ipsos, b.location_type, cast(1.0 as numeric(20,15)) as percent_edible, cast(0 as int) as iri_flag
into #brand_category_edible_by_year
from #spend_per_capita3 a
inner join #remap_category b
	on a.ipsos_category=b.category_from_data
group by ipsos_brand_name, transaction_year, b.subcategory_ipsos, b.category_ipsos, b.location_type;
-- (500 rows affected)

update #brand_category_edible_by_year 
set iri_flag=1, percent_edible=b.edible_2017
from #brand_category_edible_by_year a
inner join ahold2020.ref.iri_edible_percent b
	on a.ipsos_brand_name=b.ipsos_brand_name
	and a.year=2017;

update #brand_category_edible_by_year 
set iri_flag=1, percent_edible=b.edible_2018
from #brand_category_edible_by_year a
inner join ahold2020.ref.iri_edible_percent b
	on a.ipsos_brand_name=b.ipsos_brand_name
	and a.year=2018;

update #brand_category_edible_by_year 
set iri_flag=1, percent_edible=b.edible_2019
from #brand_category_edible_by_year a
inner join ahold2020.ref.iri_edible_percent b
	on a.ipsos_brand_name=b.ipsos_brand_name
	and a.year=2019;

update #brand_category_edible_by_year 
set iri_flag=1, percent_edible=b.edible_2020
from #brand_category_edible_by_year a
inner join ahold2020.ref.iri_edible_percent b
	on a.ipsos_brand_name=b.ipsos_brand_name
	and a.year=2020;

-- select * from #brand_category_edible_by_year where year=2017 order by location_type, category_ipsos, subcategory_ipsos, ipsos_brand_name ;

-------------------------------------------------------------------
-- create calculation table
-------------------------------------------------------------------

-- select * from #calc_county_projected_dollars;
-- drop table #calc_county_projected_dollars;
select a.transaction_year, transaction_month, state, county,
	b.location_type,
	a.ipsos_brand_name,
	b.category_ipsos,
	b.subcategory_ipsos,
	county_active_users,
	total_spend,
	total_transactions,
	total_spend/county_active_users as county_spend_per_capita,
	c.pop_estimate as census_population,
	c.pop_estimate/2.62 as census_households, --https://www.census.gov/quickfacts/fact/table/US/HCN010212 person per HH 2.62
	d.percent_edible,
	(c.pop_estimate/2.62)*d.percent_edible*(total_spend/county_active_users) as county_projected_food_dollars,
	(c.pop_estimate/2.62)*(total_spend/county_active_users) as county_projected_total_dollars
	--testing shows Sense360 spend similar to HH spend
into #calc_county_projected_dollars
from #spend_per_capita3 a
inner join #remap_category b
	on a.ipsos_category=b.category_from_data
left outer join #crma_county_census c
	on a.state=c.state_abbr
	and a.county=c.county_name
	and a.transaction_year=c.year
left outer join #brand_category_edible_by_year d
	on a.ipsos_brand_name=d.ipsos_brand_name
	and a.transaction_year=d.year;
-- (1644507 rows affected)


-------------------------------------------------------------------
-- extrapolate missing data
-------------------------------------------------------------------

-- select * from #calc_county_projected_dollars where transaction_year=2020 and transaction_month in (11,12);
-- select * from #calc_county_projected_dollars where category_ipsos in ('Grocery Delivery');
-- select * from #calc_county_projected_dollars where subcategory_ipsos in ('Pureplay Online');
-- select * from #calc_county_projected_dollars where subcategory_ipsos in ('Conglomerate Online');
-- select * from #calc_county_projected_dollars where subcategory_ipsos in ('Meal Kits');


-- INSERT EXTRAPOLATED DATA 2020 NOV/DEC
----------------------------------------
-- Note: These should always insert rows. If 0 rows inserted, there is an error somewhere.

-- Plug in Nov/Dec 2020 so that it represents 16% of the year for all categories.
-- Nov makes up 30/61 days of that 16% share
-- Dec makes up 31/61 days of that 16% share
-- the factor to multiply is not 0.16, but 0.18 since the sum is over 10 months not 12

-- select top 100 * from #calc_county_projected_dollars;
insert into #calc_county_projected_dollars
select	transaction_year, 11 as transaction_month, state, county, location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos,
	max(county_active_users) as county_active_users, 0 as total_spend, 0 as total_transactions,
	sum(county_spend_per_capita)*0.192*30/61	as county_spend_per_capita,
	max(census_population) as  census_population,
	max(census_households) as census_households,
	avg(percent_edible) as percent_edible,
	sum(county_projected_food_dollars)*0.192*30/61 as county_projected_food_dollars, 
	sum(county_projected_total_dollars)*0.192*30/61 as county_projected_total_dollars
from #calc_county_projected_dollars where transaction_year=2020 and transaction_month <=10
group by transaction_year, state, county, location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos;

insert into #calc_county_projected_dollars
select	transaction_year, 12 as transaction_month, state, county, location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos,
	max(county_active_users) as county_active_users, 0 as total_spend, 0 as total_transactions,
	sum(county_spend_per_capita)*0.192*31/61	as county_spend_per_capita,
	max(census_population) as  census_population,
	max(census_households) as census_households,
	avg(percent_edible) as percent_edible,
	sum(county_projected_food_dollars)*0.192*31/61 as county_projected_food_dollars, 
	sum(county_projected_total_dollars)*0.192*31/61 as county_projected_total_dollars
from #calc_county_projected_dollars where transaction_year=2020 and transaction_month <=10
group by transaction_year, state, county, location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos;

-- PEAPOD 2020. ADUSA DISCONTINUED PEAPOD IN SUMMER OF 2019, EXPECT DECLINE. IRI SHOWS -20%. CURRENT DATA SHOWS +20%. FACTOR ADJUSTMENT
----------------------------------------
update #calc_county_projected_dollars
set county_spend_per_capita=county_spend_per_capita*.95,
	county_projected_food_dollars=county_projected_food_dollars*.95,
	county_projected_total_dollars=county_projected_total_dollars*.95
where transaction_year=2020 and ipsos_brand_name='PeaPod';

-- FINAL 2020 ADJUSTMENT OVERALL STILL 6% LOWER
---------------------------------------
update #calc_county_projected_dollars
set county_spend_per_capita=county_spend_per_capita*1.05,
	county_projected_food_dollars=county_projected_food_dollars*1.05,
	county_projected_total_dollars=county_projected_total_dollars*1.05
where transaction_year=2020 and ipsos_brand_name<>'PeaPod' and category_ipsos<>'Grocery';

update #calc_county_projected_dollars
set county_spend_per_capita=county_spend_per_capita*1.03,
	county_projected_food_dollars=county_projected_food_dollars*1.03,
	county_projected_total_dollars=county_projected_total_dollars*1.03
where transaction_year=2020 and ipsos_brand_name<>'PeaPod' and category_ipsos='Grocery';


-- INSERT EXTRAPOLATED DATA 2017 FOOD/GROCERY/MEAL DELIVERY
----------------------------------------

-- MEALKITS HAS CONSISTENT TREND OF ABOUT 0% LIFT FROM 2017 to 2018
insert into #calc_county_projected_dollars
select 2017 as transaction_year, transaction_month, state, county, location_type,
	ipsos_brand_name, category_ipsos, subcategory_ipsos, 0 as county_active_users, total_spend, total_transactions, county_spend_per_capita,
	0 as census_population, 0 as census_households, percent_edible, county_projected_food_dollars, county_projected_food_dollars
from #calc_county_projected_dollars
where transaction_year=2018 and transaction_month in (1,2,3,4,5,6,7,8,9) and subcategory_ipsos in ('Meal Kits')

-- FOOD DELIVERY HAS CONSISTENT TREND OF ABOUT 77% LIFT FROM 2017 to 2018. DIVIDE BY 1.77 from 2018 to extrapolate 2017. NOTE: NOT MULTIPLY
insert into #calc_county_projected_dollars
select 2017 as transaction_year, transaction_month, state, county, location_type,
	ipsos_brand_name, category_ipsos, subcategory_ipsos, 0 as county_active_users, total_spend/1.77, total_transactions, county_spend_per_capita/1.77,
	0 as census_population, 0 as census_households, percent_edible, county_projected_food_dollars/1.77, county_projected_total_dollars/1.77
from #calc_county_projected_dollars
where transaction_year=2018 and transaction_month in (1,2,3,4,5,6,7,8,9) and subcategory_ipsos in ('Food Delivery')

-- Pure-play Online HAS CONSISTENT TREND OF ABOUT 43% LIFT FROM 2017 to 2018. DIVIDE BY 1.43 from 2018 to extrapolate 2017. NOTE: NOT MULTIPLY
insert into #calc_county_projected_dollars
select 2017 as transaction_year, transaction_month, state, county, location_type,
	ipsos_brand_name, category_ipsos, subcategory_ipsos, 0 as county_active_users, total_spend/1.43, total_transactions, county_spend_per_capita/1.43,
	0 as census_population, 0 as census_households, percent_edible, county_projected_food_dollars/1.43, county_projected_total_dollars/1.43
from #calc_county_projected_dollars
where transaction_year=2018 and transaction_month in (1,2,3,4,5,6,7,8) and subcategory_ipsos in ('Pure-play Online')

-- Special case where Sep 2017 has Pure-play Online data on a few counties
insert into #calc_county_projected_dollars
select 2017 as transaction_year, transaction_month, state, county, location_type,
	ipsos_brand_name, category_ipsos, subcategory_ipsos, 0 as county_active_users, total_spend/1.43, total_transactions, county_spend_per_capita/1.43,
	0 as census_population, 0 as census_households, percent_edible, county_projected_food_dollars/1.43, county_projected_total_dollars/1.43
from #calc_county_projected_dollars
where transaction_year=2018 and transaction_month = 9 and subcategory_ipsos in ('Pure-play Online')
and county not in ('Fredericksburg city',
	'Essex County',
	'Lehigh County',
	'Anne Arundel County',
	'Baltimore County',
	'Wake County',
	'Columbia County',
	'Virginia Beach city',
	'Chester County');

-- select top 100 * from #calc_county_projected_dollars;

---------------------
-- SENSE360 TO IRI DATA YEARLY CALIBRATION
---------------------
-- select * from #sense360_iri_year_calibration;
-- drop table #sense360_iri_year_calibration;
create table #sense360_iri_year_calibration (
	transaction_year int,
	calibration_factor numeric(20,5)
);

insert into #sense360_iri_year_calibration select 2017, 1.12;
insert into #sense360_iri_year_calibration select 2018, 1.05;
insert into #sense360_iri_year_calibration select 2019, 1.04;
insert into #sense360_iri_year_calibration select 2020, 1.14;

---------------------
-- CALIBRATED FOOD DOLLARS (YEAR)
---------------------
-- drop table #calc_county_projected_dollars2;
select a.transaction_year, transaction_month, state, county, location_type,
	ipsos_brand_name, category_ipsos, subcategory_ipsos,
	county_active_users,
	cast(total_spend as numeric(20,2)) as total_spend,
	total_transactions, county_spend_per_capita,
	census_population, census_households,
	cast(percent_edible as numeric(20,3)) as percent_edible,
	case when (category_ipsos='Grocery' or a.transaction_year=2017) 
		then b.calibration_factor else 1.0 end as calibration_factor, -- base calibration is only made to Grocery
	county_projected_food_dollars,
	county_projected_total_dollars
into #calc_county_projected_dollars2
from #calc_county_projected_dollars a
inner join #sense360_iri_year_calibration b
	on a.transaction_year=b.transaction_year;
-- (1737958 rows affected)

---------------------
-- CALIBRATED CATEGORY (MATCH TO IRI)
---------------------

-- Sense360 not as good as picking up long tail... IRI sizes it at 33% of USA pop, but this will be smaller among ADUSA CRMA.
-- Apply factor to increase size of "All Other Supermarket
update #calc_county_projected_dollars2 set calibration_factor=calibration_factor*1.50 where ipsos_brand_name='All Other Supermarket';

-- Sense360 shows Target as way to high (7.1B) vs IRI (2.6B when estimated to ADUSA)
-- It's possible that Targets are simply in higher concentration in ADUSA CRMA, which would cause a natural skew up, but 3x seems a bit high to be natural.
-- Apply factor to increase size of "All Other Supermarket
update #calc_county_projected_dollars2 set calibration_factor=calibration_factor*0.55 where ipsos_brand_name='Target';

-- Sense360 high for two club brands, BJ's and Costco.
update #calc_county_projected_dollars2 set calibration_factor=calibration_factor*0.85 where ipsos_brand_name='BJ''s Wholesale';
update #calc_county_projected_dollars2 set calibration_factor=calibration_factor*0.85 where ipsos_brand_name='Costco';

-- Sense360 high on convenience .
update #calc_county_projected_dollars2 set calibration_factor=calibration_factor*0.55 where category_ipsos='Convenience';

-- Sense360 high on independents.
update #calc_county_projected_dollars2 set calibration_factor=calibration_factor*0.73 where category_ipsos='Independents';

-- Sense360 low on dollar stores
update #calc_county_projected_dollars2 set calibration_factor=calibration_factor*1.2 where category_ipsos='Dollar';

-- Sense360 low on liquor
update #calc_county_projected_dollars2 set calibration_factor=calibration_factor*2.6 where category_ipsos='Liquor';


--if exists (select count(*) from #calc_county_calibrated_dollars) begin drop table #calc_county_calibrated_dollars end;
select transaction_year, transaction_month, state, county, location_type,
	ipsos_brand_name, category_ipsos, subcategory_ipsos,
	county_active_users,
	cast(total_spend as numeric(20,2)) as total_spend,
	total_transactions, county_spend_per_capita,
	census_population, census_households,
	cast(percent_edible as numeric(20,3)) as percent_edible,
	cast(county_projected_food_dollars as numeric(20,2)) as county_uncalibrated_food_dollars,
	calibration_factor,
	cast(county_projected_food_dollars*calibration_factor as numeric(20,2)) as county_calibrated_food_dollars,
	cast(county_projected_total_dollars*calibration_factor as numeric(20,2)) as county_calibrated_total_dollars
into #calc_county_calibrated_dollars
from #calc_county_projected_dollars2;


-- ==========================================================================
-- A) TREEMAP MAIN DELIVERY REPORTING
-- ==========================================================================

-- CRMA: ADUSA BY MONTH

-- select top 100 * from #calc_crma_adusa_calibrated_dollars;
-- drop table #calc_crma_adusa_calibrated_dollars;
select transaction_year as year, transaction_month as month,
	cast('ADUSA' as varchar(30)) as crma,
	location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos,
	-- sum(county_active_users) as crma_active_users,
	sum(total_spend) as total_spend,
	sum(total_transactions) as total_transactions,
	-- sum(census_population) as census_population,
	-- sum(census_households) as census_households,
	cast(avg(percent_edible) as numeric(20,3)) as percent_edible,
	sum(county_uncalibrated_food_dollars) as crma_uncalibrated_food_dollars,
	sum(county_calibrated_food_dollars) as crma_calibrated_food_dollars,
	sum(county_calibrated_total_dollars) as crma_calibrated_total_dollars
into #calc_crma_adusa_calibrated_dollars
from #calc_county_calibrated_dollars
group by transaction_year, transaction_month, location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos;

-- select top 100 * from #calc_county_calibrated_dollars;
-- select * from ref.adusa_county_crma_flag;

insert into #calc_crma_adusa_calibrated_dollars
select transaction_year as year, transaction_month as month,
	'Ahold' as crma,
	location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos,
	-- sum(county_active_users) as crma_active_users,
	sum(total_spend) as total_spend,
	sum(total_transactions) as total_transactions,
	-- sum(census_population) as census_population,
	-- sum(census_households) as census_households,
	avg(percent_edible) as percent_edible,
	sum(county_uncalibrated_food_dollars) as crma_uncalibrated_food_dollars,
	sum(county_calibrated_food_dollars) as crma_calibrated_food_dollars,
	sum(county_calibrated_total_dollars) as crma_calibrated_total_dollars
from #calc_county_calibrated_dollars a
inner join ref.adusa_county_crma_flag b
	on a.state=b.state_abbr
	and a.county=b.county_name
	and (b.stop_and_shop_flag=1 or  b.giant_food_flag=1 or b.giant_martin_flag=1)
group by transaction_year, transaction_month, location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos;

insert into #calc_crma_adusa_calibrated_dollars
select transaction_year as year, transaction_month as month,
	'Delhaize' as crma,
	location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos,
	-- sum(county_active_users) as crma_active_users,
	sum(total_spend) as total_spend,
	sum(total_transactions) as total_transactions,
	-- sum(census_population) as census_population,
	-- sum(census_households) as census_households,
	avg(percent_edible) as percent_edible,
	sum(county_uncalibrated_food_dollars) as crma_uncalibrated_food_dollars,
	sum(county_calibrated_food_dollars) as crma_calibrated_food_dollars,
	sum(county_calibrated_total_dollars) as crma_calibrated_total_dollars
from #calc_county_calibrated_dollars a
inner join ref.adusa_county_crma_flag b
	on a.state=b.state_abbr
	and a.county=b.county_name
	and (b.food_lion_flag=1 or b.hannaford_flag=1)
group by transaction_year, transaction_month, location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos;

-- select * from #calc_crma_adusa_calibrated_dollars;

-- REPLACE AMAZON FRESH DATA
----------------------------
-- drop table #calc_crma_adusa_projected_dollars2;
select year, month, crma, location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos, percent_edible,
	total_spend, total_transactions, crma_uncalibrated_food_dollars, crma_calibrated_food_dollars, crma_calibrated_total_dollars
into #calc_crma_adusa_projected_dollars2
from #calc_crma_adusa_calibrated_dollars
where ipsos_brand_name <> 'Amazon Fresh';

insert into #calc_crma_adusa_projected_dollars2
select transaction_year, transaction_month, crma,
	'In home', ipsos_brand_name, 'Grocery Delivery' as category_ipsos, 'Conglomerate Online' as subcategory_ipsos,
	1 as percent_edible, 0 as total_spend, 0 as total_transactions, 0 as crma_uncalibrated_food_dollars,
	crma_calibrated_total_dollars, crma_calibrated_total_dollars
from ahold2020.raw.iri_amazon_fresh a
where crma in ('ADUSA', 'Ahold', 'Delhaize');


-- ###############
-- OUTPUT TO EXCEL
-- ###############
select * from #calc_crma_adusa_projected_dollars2
order by crma, year, month, location_type, category_ipsos desc, subcategory_ipsos, crma_calibrated_total_dollars desc;


-- COUNTY ANNUAL
-- select * from #annual_county_calibrated_dollars;
-- drop table #annual_county_calibrated_dollars;
/***
select transaction_year as year, state, county, location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos,
	max(county_active_users) as county_active_users,
	sum(total_spend) as total_spend,
	sum(total_transactions) as total_transactions,
	sum(county_spend_per_capita) as county_spend_per_capita,
	max(census_population) as census_population,
	max(census_households) as census_households,
	avg(percent_edible) as percent_edible,
	sum(county_uncalibrated_food_dollars) as county_uncalibrated_food_dollars,
	avg(calibration_factor) as calibration_factor,
	sum(county_calibrated_food_dollars) as county_calibrated_food_dollars
	sum(county_calibrated_total_dollars) as county_calibrated_total_dollars
into #annual_county_calibrated_dollars
from #calc_county_calibrated_dollars
group by transaction_year, state, county, location_type, ipsos_brand_name, category_ipsos, subcategory_ipsos;
-- (183243 rows affected)
--(191233 rows affected)
***/

-- ### report county level ###
-- select * from #annual_county_calibrated_dollars order by year, state, county, location_type, category_ipsos, subcategory_ipsos


-- SUBCAT AGG FOR TABLEAU
-------------------------
-- drop table #calc_crma_adusa_projected_dollars2;
/***/
select year, month,
	case when month in (1,2) then 'Jan/Feb'
		else case when  month in (3,4) then 'Mar/Apr'
		else case when  month in (5,6) then 'May/Jun'
		else case when  month in (7,8) then 'Jul/Aug'
		else case when  month in (9,10) then 'Sep/Oct'
		else case when  month in (11,12) then 'Nov/Dec'
		else '' end end end end end end as bimonth,
	crma, location_type, category_ipsos, subcategory_ipsos,
	sum(total_spend) as total_spend,
	sum(total_transactions) as total_transactions,
	sum(crma_calibrated_food_dollars) as crma_calibrated_food_dollars,
	sum(crma_calibrated_total_dollars) as crma_calibrated_total_dollars
from #calc_crma_adusa_projected_dollars2
where crma='ADUSA'
group by year, month, crma, location_type, category_ipsos, subcategory_ipsos;
***/

-- END OF FILE --





