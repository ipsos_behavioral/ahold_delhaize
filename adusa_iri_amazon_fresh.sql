-- CREATE TABLE THAT REPRESENTS ESTIMATES OF AMAZON FRESH BASED ON UNIFY TOOL DATA

use ahold2020;

-- drop table ahold2020.raw.iri_amazon_fresh;
create table ahold2020.raw.iri_amazon_fresh (
	ipsos_brand_name varchar(300),
	transaction_year int,
	transaction_month int,
	crma varchar(300),
	census_population int,
	percent_of_us numeric(20,15),
	crma_calibrated_food_dollars numeric(30,2),
	crma_calibrated_total_dollars numeric(30,2)
	);

insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 1, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;
insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 2, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;
insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 3, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;
insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 4, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;
insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 5, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;
insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 6, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;
insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 7, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;
insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 8, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;
insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 9, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;
insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 10, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;
insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 11, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;
insert into ahold2020.raw.iri_amazon_fresh select 'Amazon Fresh', year, 12, 'Total US', sum(pop_estimate), 1, 0, 0 from core.geo.us_county_census_2017_2020 group by year;

update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=10037997 where transaction_year=2017 and transaction_month=1;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 9468163 where transaction_year=2017 and transaction_month=2;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=10804986 where transaction_year=2017 and transaction_month=3;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=11712913 where transaction_year=2017 and transaction_month=4;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=12231333 where transaction_year=2017 and transaction_month=5;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=10918577 where transaction_year=2017 and transaction_month=6;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=13426304 where transaction_year=2017 and transaction_month=7;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=14524379 where transaction_year=2017 and transaction_month=8;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=16044065 where transaction_year=2017 and transaction_month=9;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=17661321 where transaction_year=2017 and transaction_month=10;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=12296483 where transaction_year=2017 and transaction_month=11;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=14978902 where transaction_year=2017 and transaction_month=12;

update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 9269583 where transaction_year=2018 and transaction_month=1;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 8743370 where transaction_year=2018 and transaction_month=2;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 9977859 where transaction_year=2018 and transaction_month=3;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=10816283 where transaction_year=2018 and transaction_month=4;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=11295018 where transaction_year=2018 and transaction_month=5;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=10082754 where transaction_year=2018 and transaction_month=6;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=12398513 where transaction_year=2018 and transaction_month=7;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=13412530 where transaction_year=2018 and transaction_month=8;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=14815883 where transaction_year=2018 and transaction_month=9;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=16309338 where transaction_year=2018 and transaction_month=10;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=11355180 where transaction_year=2018 and transaction_month=11;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=13832259 where transaction_year=2018 and transaction_month=12;

update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=12964746 where transaction_year=2019 and transaction_month=1;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=12228767 where transaction_year=2019 and transaction_month=2;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=13955365 where transaction_year=2019 and transaction_month=3;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=15128013 where transaction_year=2019 and transaction_month=4;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=15797587 where transaction_year=2019 and transaction_month=5;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=14102075 where transaction_year=2019 and transaction_month=6;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=17340973 where transaction_year=2019 and transaction_month=7;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=18759210 where transaction_year=2019 and transaction_month=8;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=20721987 where transaction_year=2019 and transaction_month=9;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=22810782 where transaction_year=2019 and transaction_month=10;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=15881733 where transaction_year=2019 and transaction_month=11;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=19346257 where transaction_year=2019 and transaction_month=12;

update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 31881312 where transaction_year=2020 and transaction_month=1;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 37175216 where transaction_year=2020 and transaction_month=2;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 47853059 where transaction_year=2020 and transaction_month=3;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 61365867 where transaction_year=2020 and transaction_month=4;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 73089906 where transaction_year=2020 and transaction_month=5;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 80317338 where transaction_year=2020 and transaction_month=6;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=104817489 where transaction_year=2020 and transaction_month=7;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=104120146 where transaction_year=2020 and transaction_month=8;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 86567659 where transaction_year=2020 and transaction_month=9;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=107080784 where transaction_year=2020 and transaction_month=10;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars= 92103321 where transaction_year=2020 and transaction_month=11;
update ahold2020.raw.iri_amazon_fresh set crma_calibrated_food_dollars=100251479 where transaction_year=2020 and transaction_month=12;

update ahold2020.raw.iri_amazon_fresh set crma_calibrated_total_dollars=crma_calibrated_food_dollars where crma_calibrated_total_dollars=0;

select * from ahold2020.raw.iri_amazon_fresh;

-- drop table #crma_pops;
select year, cast('ADUSA' as varchar(30)) as crma, sum(a.pop_estimate) as pop_estimate
into #crma_pops
from core.geo.us_county_census_2017_2020 a
inner join ref.adusa_county_crma_flag b
	on a.state_abbr=b.state_abbr
	and a.county_name=b.county_name
	and adusa_flag = 1
group by a.year;

insert into #crma_pops
select year, 'Ahold' as crma, sum(a.pop_estimate) as pop_estimate
from core.geo.us_county_census_2017_2020 a
inner join ref.adusa_county_crma_flag b
	on a.state_abbr=b.state_abbr
	and a.county_name=b.county_name
	and ahold_flag = 1
group by a.year;

insert into #crma_pops
select year, 'Delhaize' as crma, sum(a.pop_estimate) as pop_estimate
from core.geo.us_county_census_2017_2020 a
inner join ref.adusa_county_crma_flag b
	on a.state_abbr=b.state_abbr
	and a.county_name=b.county_name
	and delhaize_flag = 1
group by a.year;

insert into #crma_pops
select year, 'Giant Food' as crma, sum(a.pop_estimate) as pop_estimate
from core.geo.us_county_census_2017_2020 a
inner join ref.adusa_county_crma_flag b
	on a.state_abbr=b.state_abbr
	and a.county_name=b.county_name
	and giant_food_flag = 1
group by a.year;

insert into #crma_pops
select year, 'Giant Martin''s' as crma, sum(a.pop_estimate) as pop_estimate
from core.geo.us_county_census_2017_2020 a
inner join ref.adusa_county_crma_flag b
	on a.state_abbr=b.state_abbr
	and a.county_name=b.county_name
	and giant_food_flag = 1
group by a.year;

insert into #crma_pops
select year, 'Food Lion' as crma, sum(a.pop_estimate) as pop_estimate
from core.geo.us_county_census_2017_2020 a
inner join ref.adusa_county_crma_flag b
	on a.state_abbr=b.state_abbr
	and a.county_name=b.county_name
	and food_lion_flag = 1
group by a.year;

insert into #crma_pops
select year, 'Stop & Shop' as crma, sum(a.pop_estimate) as pop_estimate
from core.geo.us_county_census_2017_2020 a
inner join ref.adusa_county_crma_flag b
	on a.state_abbr=b.state_abbr
	and a.county_name=b.county_name
	and stop_and_shop_flag = 1
group by a.year;

insert into #crma_pops
select year, 'Hannaford' as crma, sum(a.pop_estimate) as pop_estimate
from core.geo.us_county_census_2017_2020 a
inner join ref.adusa_county_crma_flag b
	on a.state_abbr=b.state_abbr
	and a.county_name=b.county_name
	and hannaford_flag = 1
group by a.year;


select a.ipsos_brand_name, a.transaction_year, a.transaction_month, b.crma,
	b.pop_estimate as census_population,
	1.0*b.pop_estimate/a.census_population as percent_of_us,
	(1.0*b.pop_estimate/a.census_population)*crma_calibrated_food_dollars as crma_calibrated_food_dollars,
	(1.0*b.pop_estimate/a.census_population)*crma_calibrated_total_dollars as crma_calibrated_total_dollars
into #crma_pops_calculated
from ahold2020.raw.iri_amazon_fresh a
inner join #crma_pops b
	on a.transaction_year=b.year
	and a.crma='Total US';

insert into ahold2020.raw.iri_amazon_fresh
select * from #crma_pops_calculated;
 
select * from ahold2020.raw.iri_amazon_fresh order by transaction_year, transaction_month, crma;

-- end of file;
